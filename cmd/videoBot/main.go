package main

import (
	"gitlab.com/tecnocchio/videobot/pkg/config"
	"gitlab.com/tecnocchio/videobot/pkg/bot"
//	"gitlab.com/tecnocchio/videobot/pkg/osys"
	"gitlab.com/tecnocchio/videobot/internal/logic"
)

func main() {

	initAll()

	f := logic.TickFunction()

	stop := logic.StartProcess(f)
	
	defer stop()
	
	select{}

	
}

func initAll(){
	config.InitConfig("videobot")
	bot.InitTelegram(config.GetPropertyStringR("TELEGRAM_TOKEN"),
		config.GetPropertyIntArray("TELEGRAM_ADMIN_CHAT_IDS"),
		config.GetPropertyLongR("TELEGRAM_NOTIFY_CHAT_ID"))
}

