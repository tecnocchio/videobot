package img

import (
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"log"
	"math"
	"net/http"
)

// compare images change
func ProcessImages(img1, img2 image.Image, threshold int, frame []int) float64 {

	var variation float64
	if len(frame) == 4 {
		// Define the focus area rectangle
		focusRect := image.Rect(frame[0], frame[1], frame[2], frame[3])
		// Extract the focus areas
		focusArea1 := extractFocusArea(img1, focusRect)
		focusArea2 := extractFocusArea(img2, focusRect)

		variation = calculateDifferences(focusArea1, focusArea2, float64(threshold))
	} else {
		variation = calculateDifferences(img1, img2, float64(threshold))
	}

	return variation
}

func GetImage(url string) (image.Image, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("bad status: %s", resp.Status)
	}

	img, _, err := image.Decode(resp.Body)
	return img, err
}

// extractFocusArea extracts a specified region from the image.
func extractFocusArea(img image.Image, rect image.Rectangle) image.Image {
	rgba := image.NewRGBA(rect.Bounds())
	draw.Draw(rgba, rect.Bounds(), img, rect.Min, draw.Src)
	return rgba.SubImage(rect)
}

// reduceResolution reduces the image resolution by averaging factorXfactor pixel blocks.
func reduceResolution(img image.Image, factor int) *image.Gray {
	bounds := img.Bounds()
	newWidth := bounds.Dx() / factor
	newHeight := bounds.Dy() / factor

	reduced := image.NewGray(image.Rect(0, 0, newWidth, newHeight))

	for y := 0; y < newHeight; y++ {
		for x := 0; x < newWidth; x++ {
			var sum int
			for dy := 0; dy < factor; dy++ {
				for dx := 0; dx < factor; dx++ {
					r, g, b, _ := img.At(bounds.Min.X+(x*factor)+dx, bounds.Min.Y+(y*factor)+dy).RGBA()
					gray := (r*299 + g*587 + b*114) / 1000
					sum += int(gray >> 8) // Convert to 8-bit grayscale
				}
			}
			avg := uint8(sum / (factor * factor))
			reduced.SetGray(x, y, color.Gray{Y: avg})
		}
	}

	return reduced
}

// generateBWImage generates a black and white image based on the difference between two images.
func generateBWImage(img1, img2 *image.Gray, threshold int) *image.Gray {
	bounds := img1.Bounds()
	if !bounds.Eq(img2.Bounds()) {
		log.Fatal("Reduced images have different sizes")
	}

	bwImg := image.NewGray(bounds)
	for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
		for x := bounds.Min.X; x < bounds.Max.X; x++ {
			diff := abs(int(img1.GrayAt(x, y).Y) - int(img2.GrayAt(x, y).Y))
			if diff > threshold {
				bwImg.Set(x, y, color.White)
			} else {
				bwImg.Set(x, y, color.Black)
			}
		}
	}

	return bwImg
}

// colorDiff calculates the Euclidean distance between two colors.
func colorDiff(c1, c2 color.Color) float64 {
	r1, g1, b1, _ := c1.RGBA()
	r2, g2, b2, _ := c2.RGBA()
	// Convert to float64 for calculation, scale from 0-65535 to 0-255
	dr := float64(r1>>8) - float64(r2>>8)
	dg := float64(g1>>8) - float64(g2>>8)
	db := float64(b1>>8) - float64(b2>>8)
	return math.Sqrt(dr*dr + dg*dg + db*db) // Euclidean distance
}

// generateBWImageColor generates a black and white image based on the color difference between two images.
func generateBWImageColor(img1, img2 image.Image, threshold float64) *image.Gray {
	bounds := img1.Bounds()
	if !bounds.Eq(img2.Bounds()) {
		log.Fatal("Images have different sizes")
	}

	bwImg := image.NewGray(bounds)
	for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
		for x := bounds.Min.X; x < bounds.Max.X; x++ {
			diff := colorDiff(img1.At(x, y), img2.At(x, y))
			if diff > threshold {
				bwImg.Set(x, y, color.White)
			} else {
				bwImg.Set(x, y, color.Black)
			}
		}
	}

	return bwImg
}

// calculateDifferences calculates the percentage of pixels in img1 that differ from img2 by more than the threshold.
func calculateDifferences(img1, img2 image.Image, threshold float64) float64 {
	bounds := img1.Bounds()
	if !bounds.Eq(img2.Bounds()) {
		log.Fatal("Images have different sizes")
	}

	totalPixels := bounds.Dx() * bounds.Dy()
	var diffCount int

	for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
		for x := bounds.Min.X; x < bounds.Max.X; x++ {
			diff := colorDiff(img1.At(x, y), img2.At(x, y))
			if diff > threshold {
				diffCount++
			}
		}
	}

	diffPercentage := (float64(diffCount) / float64(totalPixels)) * 100
	return diffPercentage
}

// abs returns the absolute value of an integer.
func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}
