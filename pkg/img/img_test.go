package img

import (
	"image"
	"image/color"
	"testing"
)

// Helper function to create a simple test image.
func createTestImage(width, height int, c color.Color) image.Image {
	img := image.NewRGBA(image.Rect(0, 0, width, height))
	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			img.Set(x, y, c)
		}
	}
	return img
}

// TestProcessImages with two identical images and expecting 0 variation.
func TestProcessImages_IdenticalImages(t *testing.T) {
	img1 := createTestImage(100, 100, color.RGBA{255, 0, 0, 255}) // Red image
	img2 := createTestImage(100, 100, color.RGBA{255, 0, 0, 255}) // Identical red image
	variation := ProcessImages(img1, img2, 10, nil)
	if variation != 0 {
		t.Errorf("Expected 0 variation, got %f", variation)
	}
}


// TestCalculateDifferences with a threshold that should detect differences.
func TestCalculateDifferences(t *testing.T) {
	img1 := createTestImage(10, 10, color.Gray{Y: 100}) // Gray image
	img2 := createTestImage(10, 10, color.Gray{Y: 110}) // Slightly different gray image
	threshold := 5.0 // Difference threshold
	diff := calculateDifferences(img1, img2, threshold)
	if diff <= 0 {
		t.Errorf("Expected a positive difference, got %f", diff)
	}
}