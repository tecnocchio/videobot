package img

import (
	"image"
	"image/jpeg"
	"log"
	"os"
	"strings"
)

// processes the images couple from files.
func Process(filePath1, filePath2 string, threshold int) float64 {
	img1, err := loadImage(filePath1)
	if err != nil {
		log.Fatalf("Error loading image 1: %v", err)
	}
	img2, err := loadImage(filePath2)
	if err != nil {
		log.Fatalf("Error loading image 2: %v", err)
	}
	var defaultFrame = []int{214, 69, 550, 161}
	variation := ProcessImages(img1, img2, threshold, defaultFrame)
	// create a snapshot only if 2 images are similar
	if variation > 1 && variation < 60 {
		writeDifferences(img1, img2, threshold, "diff_"+getOnlyFileName(filePath1))
	}
	return variation
}

// loadImage loads an image from a file.
func loadImage(filePath string) (image.Image, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	img, _, err := image.Decode(file)
	return img, err
}

func getOnlyFileName(path string) string {
	// Find the last occurrence of both separators.
	lastSlash := strings.LastIndex(path, "/")
	lastBackSlash := strings.LastIndex(path, "\\")

	// Determine the higher index to find which separator comes last.
	lastIndex := lastSlash
	if lastBackSlash > lastSlash {
		lastIndex = lastBackSlash
	}

	// If neither "/" nor "\" is found, return the original path.
	if lastIndex == -1 {
		return path
	}

	// Extract everything after the last separator.
	return path[lastIndex+1:]

}

func writeDifferences(focusArea1, focusArea2 image.Image, threshold int, fileName string) {
	bwImg := generateBWImageColor(focusArea1, focusArea2, float64(threshold))
	// Save the black and white image
	file, err := os.Create(fileName)
	if err != nil {
		log.Fatalf("Error creating output file: %v", err)
	}
	defer file.Close()
	err = jpeg.Encode(file, bwImg, &jpeg.Options{Quality: 100})
	if err != nil {
		log.Fatalf("Error saving output file: %v", err)
	}
}
