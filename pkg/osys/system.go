package osys

import (
	"os/signal"
	"syscall"
	"os"
)

func Loop() {
	// Setup channel to listen for interrupt signal
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	// Block until a signal is received
	<-c
}
