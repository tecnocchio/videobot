package bot

import (
	"log"
	"image"
	"image/jpeg" 
	"bytes"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

var bot *tgbotapi.BotAPI
var targetChats []int64

// Init telegram bot with token.
func InitTelegram(token string, admins[] int, defaultChat int64) {
	targetChats = []int64{defaultChat}

	var err error
	bot, err = tgbotapi.NewBotAPI(token)
	if err != nil {
		log.Panic(err)
	}
	bot.Debug = false
	log.Printf("Authorized on account %s", bot.Self.UserName)
	listener(admins)
}

func SendTextToChats( message string){
	for _,toChat:= range targetChats {
		SendText(toChat,message)
	}
}

func SendImageToChats( img image.Image){
	log.Printf("tochats size : %d", len(targetChats))
	for _,toChat:= range targetChats {
		log.Printf("sending image to  : %d", toChat)
		SendImage(toChat,img)
	}
}


func SendText(chatID int64, message string) {
	msg := tgbotapi.NewMessage(chatID, message)
	_, err := bot.Send(msg)
	if err != nil {
		log.Println(err)
	}
}

func SendImage(chatID int64, img image.Image) {
	var buf bytes.Buffer
	err := jpeg.Encode(&buf, img, nil) // Codifica come JPEG. Cambia se necessario.
	if err != nil {
		log.Println(err)
		return
	}

	// Crea un reader dal buffer di byte per passarlo come "file".
	reader := bytes.NewReader(buf.Bytes())
	fileBytes := tgbotapi.FileReader{Name: "image.jpg", Reader: reader, Size: int64(buf.Len())}
	photo := tgbotapi.NewPhotoUpload(chatID, fileBytes)
//	photo.FileID = photo.FileName 

	_, err = bot.Send(photo)
	if err != nil {
		log.Println(err)
	}
}