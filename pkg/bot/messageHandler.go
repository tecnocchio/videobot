package bot

import (
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"log"
	"strings"
	"strconv"
	"fmt"

)

func listener(admins []int) { 

	
	updateConfig := tgbotapi.NewUpdate(0)
	updateConfig.Timeout = 60

	updates, err := bot.GetUpdatesChan(updateConfig)
	if err != nil {
		log.Panic(err)
	}

	
	go func() {
		// read all updates
		for update := range updates {
			// Check if message
			if update.Message == nil {
				continue
			}
			senderID := update.Message.From.ID
			if !isSenderAdmin(senderID, admins) { // Check if sender is not an admin
				continue // Ignore message if sender is not an admin
			}

			chatMessage := update.Message.Text

			if !strings.HasPrefix(chatMessage, "#") {
				continue
			}

			chatID := update.Message.Chat.ID

			command := strings.ToLower(chatMessage)[1:]

			if strings.HasPrefix(command, "start") {
				targetChats=addElementUnique(targetChats, chatID)
				SendText(chatID,"starting")
			}
			if strings.HasPrefix(command, "stop") {
				targetChats=removeElementUnordered(targetChats, chatID)
				SendText(chatID,"stopping")
			}

			if strings.HasPrefix(command, "cam") {
				cam,err:=estractCamNumber(command)
				if err!=nil {
					log.Println(" error in cam estraction ")
					continue
				}
				log.Printf("cam requested : %d ",cam)
				if callbackHandler == nil {
					log.Println(" callbackHandler nil ")
					continue
				}
				callbackHandler.CamRequest(cam,chatID)
			}

		}
	}()

}

func isSenderAdmin(senderID int, admins []int) bool {
	if admins == nil {
		return true
	}
	for _, adminID := range admins {
		if senderID == adminID {
			return true // The senderID matches an admin ID, so return true
		}
	}
	return false // No match found in the loop, so return false
}

func removeElementUnordered(slice []int64, value int64) []int64 {
	for i, v := range slice {
		if v == value {
			// replace last and truncate
			slice[i] = slice[len(slice)-1]
			return slice[:len(slice)-1]
		}
	}
	return slice
}

func addElementUnique(slice []int64, value int64) []int64 {
	for _, v := range slice {
		if v == value {
			log.Println("Already started", value)
			return slice
		}
	}
	slice = append(slice, value)
	return slice
}

type CallbackHandler interface {
	CamRequest(camNum int,chatId int64)
}

var callbackHandler CallbackHandler

func RegisterOperazioniHandler(handler CallbackHandler) {
	callbackHandler = handler
}

func estractCamNumber(s string) (int, error) {  // format is "#cam1" values 0/9
	if len(s) < 4 {
		return 0, fmt.Errorf("wrong command length")
	}
	cam := s[3] 

	if cam < '0' || cam > '9' {
		return 0, fmt.Errorf("not a number")
	}
	intVal, err := strconv.Atoi(string(cam))
	if err != nil {
		return 0, err
	}

	return intVal, nil
}