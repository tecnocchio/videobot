package config

import (
	"github.com/spf13/afero"
	"os"
	"testing"
)

func TestInitConfigAndGet(t *testing.T) {
	// Setup an in-memory filesystem
	memFs := afero.NewMemMapFs()
	AppFs = memFs // Use the in-memory filesystem in tests

	// Define application name and in-memory configuration directory
	appName := "testapp"
	currDir, err := os.Getwd()
	if err != nil {
		panic("error")
	}
	cfgDirectory := currDir + "/cfg"

	// Ensure the configuration directory exists
	memFs.MkdirAll(cfgDirectory, 0755)

	// Create and write to a properties file in-memory
	propertiesContent := "key1=value1\nkey2=value2\nkey3=123"
	filePath := cfgDirectory + "/" + appName + ".properties"
	afero.WriteFile(memFs, filePath, []byte(propertiesContent), 0644)

	// Initialize configuration with the in-memory directory
	InitConfig(appName)

	// Assertions
	val1, err := GetPropertyString("key1")
	if err != nil || val1 != "value1" {
		t.Errorf("Expected 'value1', got '%s'", val1)
	}

	val2, err := GetPropertyString("key2")
	if err != nil || val2 != "value2" {
		t.Errorf("Expected 'value2', got '%s'", val2)
	}
	val3 := GetPropertyIntR("key3")
	if val3 != 123 {
		t.Errorf("Expected 123, got %d", val3)
	}
	
	// is ok if it fail :
	_,err = GetPropertyInt("key4")
	if err==nil{
		t.Errorf("Expected error")
	}

}
