package config

import (
	"bufio"
	"fmt"
	"github.com/spf13/afero"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

var AppFs afero.Fs = afero.NewOsFs() // Default to the OS filesystem

// Properties map to store the loaded properties
var properties map[string]string

func InitConfig(appName string) {
	properties = make(map[string]string)
	// Use the working directory
	workingDir, err := os.Getwd()
	if err != nil {
		log.Fatalf("Error with current working directory")
	}

	// Build the properties file path in a cross-platform way
	propertiesFilePath := filepath.Join(workingDir, "cfg", appName+".properties")

	loadProperties(propertiesFilePath)
}

// loadProperties reads the properties file and stores the key-value pairs in the properties map
func loadProperties(filePath string) {
	file, err := AppFs.Open(filePath)
	if err != nil {
		log.Fatalf("Error loading property file :%s", filePath)
		panic(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if equalIndex := strings.Index(line, "="); equalIndex >= 0 {
			key := line[:equalIndex]
			value := line[equalIndex+1:]
			properties[key] = value
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatalf("Error parsing property file :%s", filePath)
		panic(err)
	}
}

// GetPropertyString returns the string value of a property given its key
func GetPropertyString(key string) (string, error) {
	value, exists := properties[key]
	if !exists {
		// Return an empty string and an error if the key doesn't exist
		return "", fmt.Errorf("property not found for key: %s", key)
	}
	// Return the value and nil error if the key exists
	return value, nil
}

// GetPropertyString Required
func GetPropertyStringR(key string) string {
	value, err := GetPropertyString(key)
	if err != nil {
		log.Fatalf("Error with key '%s' :%v", key, err)
	}
	return value
}

// GetPropertyInt returns the int value of a property given its key
func GetPropertyInt(key string) (int, error) {
	valueStr, err := GetPropertyString(key)
	if err != nil {
		return 0, err
	} else {
		return strconv.Atoi(valueStr)
	}
}

// GetPropertyInt Required
func GetPropertyIntR(key string) int {
	intvalue, err := GetPropertyInt(key)
	if err != nil {
		log.Fatalf("Error with key '%s' :%v", key, err)
	}
	return intvalue
}

// GetPropertyLong returns the Long(int64) value of a property given its key
func GetPropertyLong(key string) (int64, error) {
	valueStr, err := GetPropertyString(key)
	if err != nil {
		return 0, err
	} else {
		num, err := strconv.ParseInt(valueStr, 10, 64)
		return num, err
	}
}

// GetPropertyLong Required
func GetPropertyLongR(key string) int64 {
	intvalue, err := GetPropertyLong(key)
	if err != nil {
		log.Fatalf("Error with key '%s' :%v", key, err)
	} 
	return intvalue   
}

// GetPropertyIntArray returns the []int value of a property given its key, assuming the value is a comma-separated list of integers.
func GetPropertyIntArray(key string) ([]int) {
	valueStr := properties[key]
	// Split the string by commas
	parts := strings.Split(valueStr, ",")
	// Prepare a slice to hold the converted integers
	ints := make([]int, len(parts))
	// Convert each part from string to int and store in the slice
	for i, part := range parts {
		var err error
		ints[i], err = strconv.Atoi(strings.TrimSpace(part)) // TrimSpace in case of spaces around commas
		if err != nil {
			return nil // Return the error if conversion fails
		}
	}
	return ints // Return the slice of integers
}

// GetPropertyLongArray returns the []int64 value of a property given its key, assuming the value is a comma-separated list of integers.
func GetPropertyLongArray(key string) ([]int64) {
	valueStr, exists := properties[key]
	if !exists {
		return nil
	}
	// Split the string by commas
	parts := strings.Split(valueStr, ",")
	// Prepare a slice to hold the converted int64s
	ints := make([]int64, len(parts))
	// Convert each part from string to int64 and store in the slice
	for i, part := range parts {
		var err error
		ints[i], err = strconv.ParseInt(strings.TrimSpace(part), 10, 64) // TrimSpace in case of spaces around commas
		if err != nil {
			return nil // Return the error if conversion fails
		}
	}
	return ints // Return the slice of int64s
}