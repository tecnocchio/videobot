module gitlab.com/tecnocchio/videobot

go 1.18

require (
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/spf13/afero v1.11.0
)

require (
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	golang.org/x/text v0.14.0 // indirect
)
