# videobot

## Overview
`videobot` is a Go application designed to monitor changes in webcam images over time and notify users of significant changes through Telegram. It operates by taking pictures from a specified webcam URL every 10 seconds, comparing the new picture with the previous one, and determining if there has been a significant change. This project is structured to separate concerns into logical units, including image processing, configuration management, and Telegram bot integration.

## Installation

### Prerequisites
- Go 1.x (where x is the latest minor version)
- A configured Telegram bot (refer to [Telegram Bot API](https://core.telegram.org/bots/api))

### Getting Started
1. Clone the repository:
    ```sh
    git clone https://gitlab.com/tecnocchio/videobot.git 
    cd videobot
    ```

2. Build the application:
   - For Windows: `./scripts/build.bat`
   - For Linux/Mac: `./scripts/build.sh`

### Configuration
1. Create a `videobot.properties` configuration file in the `./cfg` directory.
2. Populate the `videobot.properties` file with necessary configurations, including the webcam URL, Telegram bot API key, and user ID.

   Example configuration:
    ```
# required
TELEGRAM_TOKEN=111111111:AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# required
TELEGRAM_NOTIFY_CHAT_ID=111111111
# required
TRIGGER_SECONDS=10
# required
WEBCAM_URL_1=http://192.168.1.10/webcam1/getscreenshot
WEBCAM_URL_2=
WEBCAM_URL_3=
# WEBCAM_Area_1=10,10,100,100
# WEBCAM_Area_1=X,Y,X2,Y2
# empty = no frame( all pic )
WEBCAM_Area_1=
WEBCAM_Area_2=
WEBCAM_Area_3=
# pixel compare will use this soil to check that is different
# required
IMG_PROCESSING_THRESHOLD=130
# percentage of pixel changeed to raise a telegram message
# required
IMG_PROCESSING_MIN_PERCENTAGE=2
# required
IMG_PROCESSING_MAX_PERCENTAGE=80
    ```

## Usage
Run the application:
- For Windows: `./scripts/run.bat`
- For Linux/Mac: `./scripts/run.sh`

The application will start, begin monitoring the specified webcam URL, and send notifications through Telegram if significant changes are detected.

## Contributing
Contributions to `videobot` are welcome! Please submit pull requests or issues through the GitHub repository.

## Disclaimer
This software is provided "as is," without warranty of any kind. The authors and contributors are not responsible for any damages or losses that may arise from the use of this software. Always test thoroughly in a controlled environment before any production use.

## License
`videobot` is released under the GNU General Public License v3.0 (GPL-3.0). For more details, see the [LICENSE](LICENSE) file included with the source code.
