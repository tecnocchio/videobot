package logic

import (
	"fmt"
	"gitlab.com/tecnocchio/videobot/pkg/bot"
	"gitlab.com/tecnocchio/videobot/pkg/config"
	"gitlab.com/tecnocchio/videobot/pkg/img"
	"image"
	"log"
	"math/rand"
	"time"
)

type TelegramCallback struct{}

func (t *TelegramCallback) CamRequest(camNum int, chatId int64) {
	log.Printf(" Cam Request %d chat %d", camNum, chatId)
	if camUrls == nil || len(camUrls) < camNum {
		log.Printf(" Cam urls empty  ")
		return
	}
	if camNum > 0 {
		sendImage(camNum, chatId)
		return
	}
	for i := 0; i < len(camUrls); i++ {
		sendImage(i+1, chatId)
	}
}

func sendImage(camNum int, chatId int64) {
	camUrl := camUrls[camNum-1]
	currentImage, err := img.GetImage(camUrl)
	if err != nil {
		log.Printf(" error reading the image %s ", camUrl)
		return
	}
	bot.SendImage(chatId, currentImage)
}

var camUrls []string

func Setup() {
	camUrls = []string{config.GetPropertyStringR("WEBCAM_URL_1")}
	url2, err := config.GetPropertyString("WEBCAM_URL_2")
	if err == nil {
		log.Printf("reading the config for cam2 %s ", url2)
		camUrls = append(camUrls, url2)
	}
	url3, err := config.GetPropertyString("WEBCAM_URL_3")
	if err == nil {
		log.Printf("reading the config for cam3 %s ", url3)
		camUrls = append(camUrls, url3)
	}
	url4, err := config.GetPropertyString("WEBCAM_URL_4")
	if err == nil {
		log.Printf("reading the config for cam4 %s ", url4)
		camUrls = append(camUrls, url4)
	}
	handler := &TelegramCallback{}
	bot.RegisterOperazioniHandler(handler)
}

func StartProcess(cronFunction func()) func() {
	Setup()
	secondsTick := time.Second * time.Duration(config.GetPropertyIntR("TRIGGER_SECONDS"))
	ticker := time.NewTicker(secondsTick)
	stopChan := make(chan struct{})

	go func() {
		for {
			select {
			case <-ticker.C:
				cronFunction()
			case <-stopChan:
				ticker.Stop()
				return
			}
		}
	}()

	// Return a stop function
	return func() {
		log.Println("stopping tick")
		close(stopChan)
	}
}

func TickFunction() func() {

	var lastImage image.Image

	threshold := config.GetPropertyIntR("IMG_PROCESSING_THRESHOLD")

	url1 := config.GetPropertyStringR("WEBCAM_URL_1")

	minChange := float64(config.GetPropertyIntR("IMG_PROCESSING_MIN_PERCENTAGE"))

	maxChange := float64(config.GetPropertyIntR("IMG_PROCESSING_MAX_PERCENTAGE"))

	frame := config.GetPropertyIntArray("WEBCAM_Area_1")

	return func() {
		now := time.Now()
		url := fmt.Sprintf(url1+"&rand=%d", rand.Intn(10000))
		log.Printf("url:%s", url)
		currentImage, err := img.GetImage(url)
		if err != nil {
			fmt.Printf("Error fetching image: %v\n", err)
			return
		}
		if lastImage != nil {
			change := img.ProcessImages(lastImage, currentImage, threshold, frame)
			log.Println("change:", change)
			if change > minChange && change < maxChange {
				bot.SendImageToChats(currentImage)
				bot.SendTextToChats(fmt.Sprintf("threshold: %f at time : %s", change, now))

			}

		} else {
			log.Println("First call", now)
		}

		lastImage = currentImage
	}
}
